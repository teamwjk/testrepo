package com.example.gif_lib.dao;

import java.util.List;

public interface Search {

    List<String> search(String string);
}
