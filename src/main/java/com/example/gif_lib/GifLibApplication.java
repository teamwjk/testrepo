package com.example.gif_lib;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GifLibApplication {

    public static void main(String[] args) {
        SpringApplication.run(GifLibApplication.class, args);
    }
}
