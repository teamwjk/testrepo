package com.example.gif_lib.model;

import com.example.gif_lib.dao.GifDao;
import com.example.gif_lib.dao.Search;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;



@Component
public class Gif implements GifDao, Search{

    private String name;

    private Category category;

    private boolean favourite;

    private static List<Gif> allGifs = new ArrayList<>();


    static {
        Category category1 = new Category(0,"Android");
        Category category2 = new Category(1,"Funny");
        Category category3 = new Category(2,"Programming");



        allGifs.add(new Gif("android-explosion",category1,true));
        allGifs.add(new Gif("ben-and-mike",category2, false));
        allGifs.add(new Gif("book-dominos", category2, false));
        allGifs.add(new Gif("compiler-bot", category3, false));
        allGifs.add(new Gif("cowboy-coder", category3, false));
        allGifs.add(new Gif("infinite-andrew", category2, true));

    }

    public Gif(String name) {
        this.name = name;
    }

    public Gif(String name, Category category) {
        this.name = name;
        this.category = category;
    }

    public Gif(String name, Category category, boolean favourite) {
        this.name = name;
        this.category = category;
        this.favourite = favourite;
    }

    @Override
    public List<String> getUrl(List<Gif> allGifs) {
        List<String> urlList = new ArrayList<>();

        for (Gif gif : Gif.getAllGifs()) {
            String url = gif.getGifName(gif.getName());
            urlList.add(url);
        }

        return urlList;
    }

    @Override
    public String getGifName(String name) {
        name = this.getName();

        return "/gifs/" + name;
    }

    @Override
    public List<String> search(String string){
        List<String> mySearch = new ArrayList<>();

            for (Gif gif : Gif.getAllGifs()) {
                if (gif.getName().contains(string)) {
                    String result = gif.getGifName(gif.getName());
                    mySearch.add(result);
                }

            }
        for (Gif gif : Gif.getAllGifs()) {
            if (gif.getCategory().getName().contains(string)) {
                if ((mySearch.contains(gif.getGifName(gif.getName())) == false)){
                String result = gif.getGifName(gif.getName());
                mySearch.add(result);
            }
            }

        }


        return mySearch;
    }


    public List<String> findFavourites(List <Gif> allGifs){
        List<String> myFavourites = new ArrayList<>();
        for (Gif gif : Gif.getAllGifs()) {
            if (gif.isFavourite()==true) {
                String result = gif.getGifName(gif.getName());
                myFavourites.add(result);
            }

        }

        return myFavourites;
    }





    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public static List<Gif> getAllGifs() {
        return allGifs;
    }

    public boolean isFavourite() {
        return favourite;
    }

    public void setFavourite(boolean favourite) {
        this.favourite = favourite;
    }

    public Gif() {
    }
}