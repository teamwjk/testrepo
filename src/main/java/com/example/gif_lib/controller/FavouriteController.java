package com.example.gif_lib.controller;

import com.example.gif_lib.dao.GifDao;
import com.example.gif_lib.model.Gif;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class FavouriteController {

    @Autowired
    private GifDao gifDao;

    @GetMapping("/favorites")
    public String show(ModelMap map){
        map.put("gifsUrl", gifDao.findFavourites(Gif.getAllGifs()));

        return "favorites";
    }
}
